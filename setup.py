

from distutils.core import setup
import py2exe #@UnresolvedImport @UnusedImport

setup(name='Windows update clock',
      version='0.1',
      description='Update system clock of Windows system using Internet retrieved time',
      url='https://bitbucket.org/ppinard/windows_update_clock',
      author='Philippe Pinard',
      author_email='philippe.pinard@gmail.com',
      py_modules=['update_clock'],
      console=['update_clock.py'])