#!/usr/bin/env python
"""
Script to update Windows clock.
"""

# Script information for the file.
__author__ = "Philippe T. Pinard"
__email__ = "philippe.pinard@gmail.com"
__version__ = "0.1"
__copyright__ = "Copyright (c) 2015 Philippe T. Pinard"
__license__ = "GPL v3"

import ctypes as c
import json
import datetime
import urllib.request
import argparse

class _SystemTime(c.Structure):
    _fields_ = (('year', c.c_ushort),
                ('month', c.c_ushort),
                ('dayofweek', c.c_ushort),
                ('day', c.c_ushort),
                ('hour', c.c_ushort),
                ('minute', c.c_ushort),
                ('second', c.c_ushort),
                ('milliseconds', c.c_ushort),
                )

def get_world_clock(url):
    request = urllib.request.urlopen(url)
    data = json.loads(request.read().decode('ascii'))
    return datetime.datetime.strptime(data['datetime'],
                                      '%a, %d %b %Y %H:%M:%S %z')

def set_system_clock(dt):
    st = _SystemTime(dt.year, dt.month, dt.weekday(), dt.day,
                     dt.hour, dt.minute, dt.second, dt.microsecond)
    retcode = c.windll.kernel32.SetSystemTime(c.byref(st)) #@UndefinedVariable
    if retcode == 0:
        raise RuntimeError('Could not set time. Maybe run script as administrator')

def run():
    parser = argparse.ArgumentParser(description='Update system clock of Windows system using Internet retrieved time')
    parser.add_argument('--url',
                        help='URL to retrieve JSON containing current UTC time (default: http://json-time.appspot.com/time.json)',
                        default='http://json-time.appspot.com/time.json')

    args = parser.parse_args()

    now = get_world_clock(args.url)
    set_system_clock(now)

if __name__ == '__main__':
    run()
